/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal_sys_param.h"
#include <securec.h>


static const char OHOS_DEVICE_TYPE[] = {"Development Board"};
static const char OHOS_DISPLAY_VERSION[] = {"OpenHarmony-master"};
static const char OHOS_PRODUCT_TYPE[] = {"linkiot"};
static const char OHOS_MANUFACTURE[] = {"Bouffalolab"};
static const char OHOS_BRAND[] = {"bouffalolab"};  //?��??
static const char OHOS_MARKET_NAME[] = {"BL602"}; //2��?�� name
static const char OHOS_PRODUCT_SERIES[] = {"bl602"};
static const char OHOS_PRODUCT_MODEL[] = {"bl602"};
static const char OHOS_SOFTWARE_MODEL[] = {"BL602"};
static const char OHOS_HARDWARE_MODEL[] = {"BL602"};
static const char OHOS_HARDWARE_PROFILE[] = {"{RAM:352K,ROM:288K,WIFI:true}"};
static const char OHOS_BOOTLOADER_VERSION[] = {"u-boot-v2019.07"};
static const char OHOS_SECURITY_PATCH_TAG[] = {"2022-03-22"};
static const char OHOS_ABI_LIST[] = {"riscv-liteos"};
static const char OHOS_SERIAL[] = {"AA34567890"};  // provided by OEM.
static const int OHOS_FIRST_API_VERSION = 1;

/*static const char OHOS_PRODUCT_TYPE[] = {"linkiot"};
static const char OHOS_MANUFACTURE[] = {"SmartAiLife"};
static const char OHOS_BRAND[] = {"hf"};
static const char OHOS_MARKET_NAME[] = {"****"};
static const char OHOS_PRODUCT_SERIES[] = {"k2shf"};
static const char OHOS_PRODUCT_MODEL[] = {"k2shf"};
static const char OHOS_SOFTWARE_MODEL[] = {"LPT260-JYV3-20"};
static const char OHOS_HARDWARE_MODEL[] = {"LPT260"};
static const char OHOS_HARDWARE_PROFILE[] = {"{RAM:352K,ROM:288K,WIFI:true}"};
static const char OHOS_BOOTLOADER_VERSION[] = {"u-boot-v2019.07"};
static const char OHOS_SECURITY_PATCH_TAG[] = {"2020-12-01"};
static const char OHOS_ABI_LIST[] = {"riscv-liteos"};
static const char OHOS_SERIAL[] = {"1234567890"};  // provided by OEM.*/

const char* HalGetDeviceType(void)
{
    return OHOS_DEVICE_TYPE;
}

const char* HalGetDisplayVersion(void)
{
    return OHOS_DISPLAY_VERSION;
}

const char* HalGetIncrementalVersion(void)
{
    return INCREMENTAL_VERSION;
}

const char* HalGetBuildType(void)
{
    return BUILD_TYPE;
}

const char* HalGetBuildUser(void)
{
    return BUILD_USER;
}

const char* HalGetBuildHost(void)
{
    return BUILD_HOST;
}

const char* HalGetBuildTime(void)
{
    return BUILD_TIME;
}

int HalGetFirstApiVersion(void)
{
    return OHOS_FIRST_API_VERSION;
}

char* HalGetProductType(void)
{
    char* value = (char*)malloc(strlen(OHOS_PRODUCT_TYPE) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_PRODUCT_TYPE) + 1, OHOS_PRODUCT_TYPE) != 0) {
        free(value);
        return NULL;
    }
    return value;
}

const char* HalGetManufacture(void)
{
    char* value = (char*)malloc(strlen(OHOS_MANUFACTURE) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_MANUFACTURE) + 1, OHOS_MANUFACTURE) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetBrand(void)
{
    char* value = (char*)malloc(strlen(OHOS_BRAND) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_BRAND) + 1, OHOS_BRAND) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetMarketName(void)
{
    char* value = (char*)malloc(strlen(OHOS_MARKET_NAME) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_MARKET_NAME) + 1, OHOS_MARKET_NAME) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetProductSeries(void)
{
    char* value = (char*)malloc(strlen(OHOS_PRODUCT_SERIES) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_PRODUCT_SERIES) + 1, OHOS_PRODUCT_SERIES) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetProductModel(void)
{
    char* value = (char*)malloc(strlen(OHOS_PRODUCT_MODEL) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_PRODUCT_MODEL) + 1, OHOS_PRODUCT_MODEL) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetSoftwareModel(void)
{
    char* value = (char*)malloc(strlen(OHOS_SOFTWARE_MODEL) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_SOFTWARE_MODEL) + 1, OHOS_SOFTWARE_MODEL) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetHardwareModel(void)
{
    char* value = (char*)malloc(strlen(OHOS_HARDWARE_MODEL) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_HARDWARE_MODEL) + 1, OHOS_HARDWARE_MODEL) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetHardwareProfile(void)
{
    char* value = (char*)malloc(strlen(OHOS_HARDWARE_PROFILE) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_HARDWARE_PROFILE) + 1, OHOS_HARDWARE_PROFILE) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetSerial(void)
{
#if 0
    unsigned char mac[6]={0};
    extern int hfwifi_read_sta_mac_address(unsigned char *mac);
    hfwifi_read_sta_mac_address(mac);
    char serial[16]={0};
    sprintf(serial,"%02X%02X%02X%02X%02X%02X",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
    char* value = (char*)malloc(strlen(serial) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(serial) + 1, serial) != 0) 
    {
        free(value);
        return NULL;
    }
    return (const char *)value;
#endif
    return OHOS_SERIAL;
}

const char* HalGetBootloaderVersion(void)
{
    char* value = (char*)malloc(strlen(OHOS_BOOTLOADER_VERSION) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_BOOTLOADER_VERSION) + 1, OHOS_BOOTLOADER_VERSION) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetSecurityPatchTag(void)
{
    char* value = (char*)malloc(strlen(OHOS_SECURITY_PATCH_TAG) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_SECURITY_PATCH_TAG) + 1, OHOS_SECURITY_PATCH_TAG) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}

const char* HalGetAbiList(void)
{
    char* value = (char*)malloc(strlen(OHOS_ABI_LIST) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_ABI_LIST) + 1, OHOS_ABI_LIST) != 0) {
        free(value);
        return NULL;
    }
    return (const char *)value;
}
